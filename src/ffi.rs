#![allow(non_snake_case, dead_code)]
use widestring::WideCString;

#[link(name = "vlock")]
extern "C" {
    fn IsKHookInstalled() -> bool;
    fn InstallKHook() -> bool;
    fn UninstallKHook() -> bool;
    fn SetKHookConfig(config: *mut KHConfig);

    fn DisableTskMan() -> bool;
    fn RestoreTskMan() -> bool;
}

#[repr(C)]
pub struct KHConfig {
    pub ClearCountsAsFail: bool,
    pub RequireEnter: bool,
    pub PwLength: u16,
    pub Pw: WideCString,

    pub OnInput: extern "C" fn(input_buffer: WideCString, size: u16, full_input: WideCString),
    pub OnSuccess: extern "C" fn(),
    pub OnFail: extern "C" fn(pw: WideCString),
}

pub fn install_key_hook() -> bool {
    unsafe { InstallKHook() }
}

pub fn uninstall_key_hook() -> bool {
    unsafe { UninstallKHook() }
}

pub fn is_key_hook_installed() -> bool {
    unsafe { IsKHookInstalled() }
}

pub fn set_key_hook_config(config: &mut KHConfig) {
    unsafe { SetKHookConfig(config as *mut KHConfig) }
}

pub fn disable_task_manager() -> bool {
    unsafe { DisableTskMan() }
}

pub fn restore_task_manager() -> bool {
    unsafe { RestoreTskMan() }
}

pub extern "C" fn on_input(input_buffer: WideCString, size: u16, full_input: WideCString) {
    println!("Input buffer: {:?}", input_buffer);
    println!("Size: {:?}", size);
    println!("Full input: {:?}", full_input);
}

pub extern "C" fn on_fail(pw: WideCString) {
    println!("Failed to unlock with password: {:?}", pw);
}

pub extern "C" fn on_success() {
    println!("Successfully unlocked!");
}
