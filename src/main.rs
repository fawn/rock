use widestring::widecstr;

mod ffi;

fn main() {
    ffi::set_key_hook_config(&mut ffi::KHConfig {
        ClearCountsAsFail: false,
        RequireEnter: false,
        PwLength: 4,
        Pw: widecstr!("1234").to_owned(),

        OnInput: ffi::on_input,
        OnSuccess: ffi::on_success,
        OnFail: ffi::on_fail,
    });

    ffi::install_key_hook();
    println!("is key hook installed: {}", ffi::is_key_hook_installed());

    // ffi::disable_task_manager();

    std::thread::sleep(std::time::Duration::from_secs(3));
    // ffi::restore_task_manager();
    ffi::uninstall_key_hook();
    println!("is key hook installed: {}", ffi::is_key_hook_installed());
}
